import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import {
  createTheme,
  ThemeProvider,
  CssBaseline,
  colors,
  Container,
} from "@mui/material";

import HomePage from "./pages/home.page";

function App() {
  const theme = createTheme({
    palette: {
      background: {
        default: colors.deepPurple[50],
      },
      primary: {
        main: colors.deepPurple[500],
      }
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Router>
        <Container
          maxWidth="xl"
          sx={{
            height: "100vh",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Switch>
            <Route path="/" exact><HomePage /></Route>
          </Switch>
        </Container>
      </Router>
    </ThemeProvider>
  );
}

export default App;
import { useEffect, useState } from "react";

import {
    Container,
    Card,
    CardHeader,
    CardContent,
} from "@mui/material";

import Axios from "axios";

const HomePage = () => {
    const env = process.env;

    return (
        <Container
            maxWidth="md"
        >
            <Card
                variant="elevation"
                elevation={20}
                sx={{
                    borderRadius: 1,
                }}
            >
                <CardHeader
                    title="SMS Logger Service"
                    sx={{
                        color: "primary.main",
                        borderBottom: "solid",
                        borderBottomWidth: 1,
                        borderBottomColor: "primary.main",
                    }}
                />
                <CardContent>
                </CardContent>
            </Card>
        </Container>
    );
}

export default HomePage;